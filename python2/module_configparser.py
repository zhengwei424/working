#!/usr/bin/python
# -*- coding: utf-8 -*-

import ConfigParser

# 方法：
# sections ：返回一个包含所有章节的列表；
# has_section ：判断章节是否存在
# items ：以元组的形式返回所有选项；
# options ：返回 个包含章节下所有选项的列表；
# has_option ：判断某个选项是否存在；
# get    getboolean     getinit       getfloat ：获取选项的值

# remove section ：删除 个章节；
# add section ：添加 个章节；
# remote_option ：删除 个选项；
# set ：添加 个选项；
# write ConfigParser 对象中的数据保存到文件中

cf = ConfigParser.ConfigParser(allow_no_value="true")

cf.read("/root/my.cnf")
print cf.sections()
print cf.has_section("client")
print cf.has_section("mysqld")
print cf.items("mysqld")
print cf.items("client")
print cf.items("mysqld")
print cf.options("client")
print cf.options("mysqld")
print cf.has_option("mysqld", "tmpdir")
print cf.get("client", "password")
