#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import print_function
import fileinput

# 可以读取标准输入，也可以直接读取文件内容，也可以同时读取多个文件
# 例如：
# cat /etc/passwd I python read fr le npu t.p
# python read_from_fileinput .py < /etc/passwd
# __python read_from_fileinput .py /etc/passwd
# __python read_from_fileinput .py /etc/passwd /etc/hosts


# 常用方法：
# filename 当前正在读取的文件名；
# fileno ：文件的描述符；
# filelineno ：正在读取的行是当前文件的第几行；
# isfirstline 正在读取的行是否当前文件的第 行；
# isstdin fileinput ：正在读取文件还是直接从标准输入读取内容

for line in fileinput.input():
    meta = [
        fileinput.filename(),
        fileinput.fileno(),
        fileinput.filelineno(),
        fileinput.isfirstline(),
        fileinput.isstdin()
    ]
    print(*meta, end="")
    print("  ", end="")
    print(line, end="")
