#!/usr/bin/python
# -*- coding: utf-8 -*-

# stderr 一般用于输出程序错误信息，返回一个非零值，退出程序
import sys

# sys.stdout.write("out")
sys.stderr.write("error message")
sys.exit(2)
